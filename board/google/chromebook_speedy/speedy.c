/*
 * (C) Copyright 2016 Raptor Engineering, LLC
 * (C) Copyright 2015 Google, Inc
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */

#include <common.h>
#include <syscon.h>
#include <asm/io.h>
#include <asm-generic/gpio.h>
#include <asm/arch/grf_rk3288.h>
#include <asm/arch/pmu_rk3288.h>
#include <asm/arch-rockchip/clock.h>
#include <asm/arch-rockchip/gpio.h>
#include <asm/arch-rockchip/tzpc.h>
#include <power/pmic.h>
#include <power/regulator.h>

static int control_usb_power(bool on)
{
	int ret;

	debug("%s: turn on: %d\n", __func__, on);

	ret = gpio_request(ROCKCHIP_GPIO_NR(0, 'B', 3), "host1_power_en");
	if (ret) {
		error("Unable to request HOST1 power enable GPIO.  Error: %d\n", ret);
		return ret;
	}
	ret = gpio_request(ROCKCHIP_GPIO_NR(0, 'B', 4), "usb_otg_power_en");
	if (ret) {
		error("Unable to request USB OTG power enable GPIO.  Error: %d\n", ret);
		return ret;
	}
	ret = gpio_request(ROCKCHIP_GPIO_NR(7, 'C', 5), "5v_drive_enable");
	if (ret) {
		error("Unable to request 5V drive enable GPIO.  Error: %d\n", ret);
		return ret;
	}

	gpio_direction_output(ROCKCHIP_GPIO_NR(0, 'B', 3), on);	/* HOST1 power enable */
	gpio_direction_output(ROCKCHIP_GPIO_NR(0, 'B', 4), on);	/* USB OTG power enable */
	gpio_direction_output(ROCKCHIP_GPIO_NR(7, 'C', 5), on);	/* 5V drive enable */

	return 0;
}

static int control_sdcard_power(bool on)
{
	struct udevice *vccio_dev;
	struct udevice *vcc33_dev;
	int ret;

	debug("%s: turn on: %d\n", __func__, on);

	ret = regulator_get_by_platname("vccio_sd", &vccio_dev);
	if (ret) {
		error("Unable to locate IO Vcc regulator.  Error: %d\n", ret);
		return ret;
	}
	ret = regulator_get_by_platname("vcc33_sd", &vcc33_dev);
	if (ret) {
		error("Unable to locate main Vcc regulator.  Error: %d\n", ret);
		return ret;
	}

	if (on) {
		ret = regulator_set_enable(vccio_dev, true);
		if (!ret)
			ret = regulator_set_value(vccio_dev, 3300000);
		if (!ret)
			ret = regulator_set_enable(vcc33_dev, true);
		if (!ret)
			ret = regulator_set_value(vcc33_dev, 3300000);
		if (ret)
			error("Unable to configure regulator(s)!  Error: %d\n", ret);
	} else {
		ret = regulator_set_enable(vccio_dev, false);
		if (!ret)
			ret = regulator_set_enable(vcc33_dev, false);
		if (ret)
			error("Unable to deactivate regulator(s)!  Error: %d\n", ret);
	}

	debug("%s: current IO Vcc status: %d %d\n", __func__, regulator_get_value(vccio_dev), regulator_get_enable(vccio_dev));
	debug("%s: current main Vcc status: %d %d\n", __func__, regulator_get_value(vcc33_dev), regulator_get_enable(vcc33_dev));

	return ret;
}

static int board_sdcard_init(void)
{
	int ret;

	/* Enable main USB power rails (SD power is derived from USB power) */
	ret = control_usb_power(true);
	if (ret) {
		debug("%s: board_sdcard_init() failed with error code %d\n", __func__, ret);
		return ret;
	}

	/* Enable SD card slot power */
	return control_sdcard_power(true);
}

int power_init_board(void)
{
	int ret;

	ret = board_sdcard_init();
	if (ret)
		debug("%s: board_sdcard_init() failed with error code %d\n", __func__, ret);

	return ret;
}

#ifdef CONFIG_ARMV7_NONSEC
void armv7_nonsec_tzpc_init(void)
{
	struct rockchip_tzpc *tzpc;
	tzpc = (struct rockchip_tzpc *)TZPC_BASE_OFFSET;

	debug("%s: Using TZPC base address of %08x to initialize non-secure peripheral access\n", __func__, TZPC_BASE_OFFSET);

	writel(R0SIZE, &tzpc->r0size);

	writel(0xff, &tzpc->decprot0set);
	writel(0xff, &tzpc->decprot1set);
	writel(0xff, &tzpc->decprot2set);

	debug("%s: PERIPHID0/PCELLID0: %08x/%08x (TZPCDECPROT0STAT: %08x)\n", __func__, readl(&tzpc->periphid0), readl(&tzpc->pcellid0), readl(&tzpc->decprot0stat));
	debug("%s: PERIPHID1/PCELLID1: %08x/%08x (TZPCDECPROT1STAT: %08x)\n", __func__, readl(&tzpc->periphid1), readl(&tzpc->pcellid1), readl(&tzpc->decprot1stat));
	debug("%s: PERIPHID2/PCELLID2: %08x/%08x (TZPCDECPROT2STAT: %08x)\n", __func__, readl(&tzpc->periphid2), readl(&tzpc->pcellid2), readl(&tzpc->decprot2stat));
	debug("%s: PERIPHID3/PCELLID3: %08x/%08x\n", __func__, readl(&tzpc->periphid3), readl(&tzpc->pcellid3));
}

void smp_set_core_boot_addr(unsigned long addr, int corenr)
{
	uint32_t val;
	struct rk3288_pmu *pmu;
	phys_addr_t regs = 0xff700000;

	debug("%s: Using SMP base RAM address of %08x to configure CPU %d start vector %08x\n", __func__, (unsigned int)regs, corenr, (unsigned int)addr);

	/* Turn on core power */
	pmu = syscon_get_first_range(ROCKCHIP_SYSCON_PMU);

	debug("%s: Enabling core power via PMU at %08x\n", __func__, (unsigned int)&pmu->pwrdn_con);

	val = readl(&pmu->pwrdn_con);
	val &= ~0xe;	/* Enable all three secondary cores */
	writel(val, &pmu->pwrdn_con);

	/* Write SMP start address */
	writel(addr, regs + 8);
	writel(0xdeadbeaf, regs + 4);

	/* make sure this write is really executed */
	__asm__ volatile ("dsb\n");

	debug("%s: Done\n", __func__);
}
#endif
